import {ActionReducerMap} from '@ngrx/store';
import {Cita} from './citas/models/cita.model';
import {citaReducer} from './citas/cita.reducer';
import {filtersValid} from './filter/filter.actions';
import {filterReducer} from './filter/filter.reducer';


export interface AppState {
    citas: Cita[],
    filter: filtersValid
}

export const appReducers: ActionReducerMap<AppState>={
    citas: citaReducer,
    filter: filterReducer
}
