import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {TodoModule} from './citas/cita.module';
import {CitaAddComponent} from './citas/cita-add/cita-add.component';

import { ReactiveFormsModule } from '@angular/forms';

//NgRx
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {environment} from '../environments/environment';
import {appReducers} from './app.reducer';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  declarations: [
    AppComponent
    
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    TodoModule,
    MatCheckboxModule,
    StoreModule.forRoot(  appReducers ),
    StoreDevtoolsModule.instrument( {
      maxAge: 25,
      logOnly: environment.production
    }),
    BrowserAnimationsModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
