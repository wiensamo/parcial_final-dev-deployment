import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.reducer';
import * as actions from '../cita.actions';

@Component({
  selector: 'app-cita-add',
  templateUrl: './cita-add.component.html',
  styleUrls: ['./cita-add.component.css']
})
export class CitaAddComponent implements OnInit {

  txtInput: FormControl;

  constructor( private store: Store<AppState>) {
    this.txtInput = new FormControl('', Validators.required);
  }

  ngOnInit(): void {
  }

  add() {
    if (this.txtInput.invalid ) { return ;  }
    this.store.dispatch( actions.create( { text: this.txtInput.value } ) );

    this.txtInput.reset();
  }

}
