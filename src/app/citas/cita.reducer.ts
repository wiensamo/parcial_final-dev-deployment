import {createReducer, on} from '@ngrx/store';
import * as accion from './cita.actions';
import {Cita} from './models/cita.model';




export const initialState: Cita[]= [];

const _citaReducer = createReducer ( initialState,
    on(accion.create, (state, { text }) => [...state, new Cita( text )]),
    on(accion.toggle, (state, { id }) => {
        return state.map( cita => {
          if ( cita.id === id ) {
            return {
              ...cita,
              completed: !cita.completed
            };
          }  else {
            return cita;
          }
        });
      }),

      on(accion.edit, (state, { id, nombre }) => {
        return state.map( cita => {
          if ( cita.id === id ) {
            return {
              ...cita,
              nombre
            };
          }  else {
            return cita;
          }
        });
      }),

      on(accion.remove, (state, { id }) =>  state.filter( cita => cita.id !== id )),

      on(accion.toggleAll, (state, { completed }) => {
        return state.map( cita => {
            return {
              ...cita,
              completed
            };
        });
      }),
      on(accion.clearComplete, (state) => state.filter( cita => !cita.completed ))
      
    );

    export function citaReducer(state,action){
        return _citaReducer(state, action);
    }
