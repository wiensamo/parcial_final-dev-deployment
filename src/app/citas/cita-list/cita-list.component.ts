import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.reducer';
import {Cita} from '../models/cita.model';
import {filtersValid} from '../../filter/filter.actions';

@Component({
  selector: 'app-cita-list',
  templateUrl: './cita-list.component.html',
  styleUrls: ['./cita-list.component.css']
})
export class CitaListComponent implements OnInit {

  citas: Cita[] = [];
  filterCurrent: filtersValid;

  constructor( private store: Store<AppState> ) {
    this.store.subscribe(({ citas,filter }) => {this.citas = citas; this.filterCurrent = filter;} );
   }


  ngOnInit(): void {
  }
}
