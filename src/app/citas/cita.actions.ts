import {createAction, props} from '@ngrx/store';


export const create = createAction(
    '[CITA] Create cita',
          props<{ text: string }>()
  );

  export const toggle = createAction(
    '[CITA] Toggle cita',
    props<{ id: number }>()
  );

  export const edit = createAction(
    '[CITA] Edit cita',
    props<{ id: number, nombre: string }>()
  );

  export const remove = createAction(
    '[CITA] Remove cita',
    props<{ id: number }>()
  );
  
  export const toggleAll = createAction(
    '[CITA] Toggle All cita',
    props<{ completed: boolean }>()
  );
  
  export const clearComplete = createAction('[CITA] Clear Complete todo');