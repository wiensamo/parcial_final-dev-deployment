import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import {CitaAddComponent} from './cita-add/cita-add.component';
import { ReactiveFormsModule } from '@angular/forms';
import {CitaItemComponent} from './citas-item/cita-item.component';
import {TodoPageComponent} from './todo-page/todo-page.component';
import { CitaListComponent } from './cita-list/cita-list.component';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
 

@NgModule({
    declarations: [CitaAddComponent, TodoPageComponent, CitaListComponent, CitaItemComponent],
    exports: [
      TodoPageComponent,
      CitaAddComponent,
      CitaListComponent,
      CitaItemComponent
    ],
    imports: [
      CommonModule,
      ReactiveFormsModule,
      MatCheckboxModule,
      MatButtonModule,
      MatListModule,
      MatCardModule,
      MatToolbarModule,
      MatFormFieldModule,
      MatInputModule
    ]
  })
  export class TodoModule { }
  