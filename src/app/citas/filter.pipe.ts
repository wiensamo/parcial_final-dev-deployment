import { Pipe, PipeTransform } from '@angular/core';
import {Cita} from './models/cita.model';
import {filtersValid} from '../filter/filter.actions';

@Pipe({
  name: 'filterTodo'
})
export class FilterPipe implements PipeTransform {

  transform(citas: Cita[], filter: filtersValid): Cita[] {

    switch ( filter ) {
      case 'completed':
        return citas.filter( cita => cita.completed );

      case 'pending':
        return citas.filter( cita => !cita.completed );

      default:
        return citas;
    }
  }

}