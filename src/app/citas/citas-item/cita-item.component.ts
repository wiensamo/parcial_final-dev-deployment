import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Cita} from '../models/cita.model';
import {FormControl, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.reducer';
import * as actions from '../cita.actions';

@Component({
  selector: 'app-cita-item',
  templateUrl: './cita-item.component.html',
  styleUrls: ['./cita-item.component.css']
})
export class CitaItemComponent implements OnInit {

 @ViewChild('inputControl') txtInputControl: ElementRef;
 @Input() cita: Cita;

// cita: Cita;

  chkCompleted: FormControl;
  txtInput: FormControl;

  editing = false;
  

  constructor( private store: Store<AppState> ) { }

  ngOnInit(): void {
    this.chkCompleted = new FormControl( this.cita.completed );
    this.txtInput = new FormControl( this.cita.nombre, Validators.required );

    this.chkCompleted.valueChanges.subscribe( valor => {
      this.store.dispatch( actions.toggle( { id: this.cita.id } ) );
    });

  }

  edit() {
    this.editing = true;
    this.txtInput.setValue( this.cita.nombre);

    setTimeout(() => {
      this.txtInputControl.nativeElement.select();
    }, 1);
  }

  save() {
    this.editing = false;

    if ( this.txtInput.invalid ) { return; }
    if ( this.txtInput.value === this.cita.nombre ) { return; }

    this.store.dispatch(
      actions.edit({
        id: this.cita.id,
        nombre: this.txtInput.value,
      })
    );
  }

  remove() {
    this.store.dispatch( actions.remove( { id: this.cita.id } ) );
  }

}
