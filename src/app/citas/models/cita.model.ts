
export class Cita {
    public id: number;
    public nombre: string;
    public descrip: string;
    public completed: boolean;
  
    constructor( text: string ) {
      this.nombre = text;
      this.descrip = text;
      this.id = Math.random();
      this.completed = false;
    }
  }
  